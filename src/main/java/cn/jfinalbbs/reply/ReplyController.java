package cn.jfinalbbs.reply;

import cn.jfinalbbs.collect.Collect;
import cn.jfinalbbs.common.BaseController;
import cn.jfinalbbs.common.Constants;
import cn.jfinalbbs.interceptor.UserInterceptor;
import cn.jfinalbbs.notification.Notification;
import cn.jfinalbbs.topic.Topic;
import cn.jfinalbbs.user.User;
import cn.jfinalbbs.utils.StrUtil;
import com.jfinal.aop.Before;

import java.util.Date;
import java.util.List;

/**
 * Created by liuyang on 15/4/3.
 */
public class ReplyController extends BaseController {

    @Before(UserInterceptor.class)
    public void index() {
        String tid = getPara(0);
        Date date = new Date();
        Topic topic = Topic.me.findById(tid);
        User sessionUser = getSessionAttr(Constants.USER_SESSION);
        User user = User.me.findById(sessionUser.get("id"));
        if (topic == null) {
            renderText(Constants.OP_ERROR_MESSAGE);
        } else {
            // 增加1积分
            Reply reply = new Reply();
            String quote = getPara("quote");
            String content = getPara("content");
            reply.set("id", StrUtil.getUUID())
                    .set("tid", tid)
                    .set("content", content)
                    .set("in_time", date)
                    .set("quote", 0)
                    .set("author_id", user.get("id"));
            Reply r = Reply.me.findById(quote);
            if (r != null) {
                User quote_user = User.me.findById(r.get("author_id"));
                reply.set("quote", quote)
                        .set("quote_content", r.get("content"))
                        .set("quote_author_nickname", quote_user.get("nickname"));
                // 通知话题发布者
                Notification replyNoti = new Notification();
                replyNoti.set("tid", tid)
                        .set("rid", reply.get("id"))
                        .set("read", 0)
                        .set("message", Constants.NOTIFICATION_MESSAGE2)
                        .set("from_author_id", user.get("id"))
                        .set("author_id", r.get("author_id"))
                        .set("in_time", date).save();
            }
            reply.save();
            //话题最后回复时间更新
            topic.set("last_reply_time", date).set("last_reply_author_id", user.get("id")).update();
            //用户积分增加
            user.set("score", user.getInt("score") + 1).update();
            //引用回复且是自己的话题的话，不用发送通知
            if (!user.get("id").equals(topic.get("author_id"))) {
                // 通知话题发布者
                Notification topicNoti = new Notification();
                topicNoti.set("tid", tid)
                        .set("rid", reply.get("id"))
                        .set("read", 0)
                        .set("message", Constants.NOTIFICATION_MESSAGE1)
                        .set("from_author_id", user.get("id"))
                        .set("author_id", topic.get("author_id"))
                        .set("in_time", date).save();
            }
            // 通知关注该话题的人
            List<Collect> collectList = Collect.me.findByTid(tid);
            for(Collect c: collectList) {
                // 通知话题发布者
                Notification collectNoti = new Notification();
                collectNoti.set("tid", tid)
                        .set("rid", reply.get("id"))
                        .set("read", 0)
                        .set("message", Constants.NOTIFICATION_MESSAGE3)
                        .set("from_author_id", user.get("id"))
                        .set("author_id", c.get("author_id"))
                        .set("in_time", date).save();
            }
            setSessionAttr(Constants.USER_SESSION, user);
            redirect(Constants.getBaseUrl() + "/topic/" + tid + ".html" + "#" + reply.get("id"));
        }
    }
}
